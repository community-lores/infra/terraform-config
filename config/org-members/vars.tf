# Variables for documentation and stuff.

variable "GITLAB_TF_STATE_USER" {
  description = "GitLab SaaS username for Terraform Managed State"
  type = string
}

variable "GITLAB_TF_STATE_PAT" {
  description = "GitLab SaaS PAT for Terraform Managed State"
  sensitive = true
  type = string
}

variable "GITLAB_TOKEN" {
  description = "GitLab SaaS PAT of an Community Lores Project Coordinator or maintainer."
  sensitive = true
}

variable "GITHUB_TOKEN" {
  description = "GitHub PAT of an organization owner at Community Lores GitHUb org, usually project coordinators only."
  sensitive = true
}

