# Global configuration for managing org members stuff.

terraform {
  backend "http" {
    address = "https://gitlab.com/api/v4/"
    username = var.GITLAB_TF_STATE_USER
    password = var.GITLAB_TF_STATE_PAT
  }
  required_providers {
    github = {
      source = "integrations/github"
      version = "4.17.0"
    }
    gitlab = {
      source = "gitlabhq/gitlab"
      version = "3.7.0"
    }
  }
}
