# Infra As Code - Terraforming

Terraform configuration files for managing resources at Community Lores [GitHub](https://github.com/community-lores) + [GitLab SaaS](https://gitlab.com/community-lores) org.

## Prerequisites

* GitLab SaaS and GitHub.com PATs for API authenication on performing `terraform plan` and `terraform apply`, preferrly our shared team account's PATs to esclate permissions.[^1]
* Another GitLab SaaS PAT for managing remote state and running `terraform init` through the `scripts/setup` script. Only people with atleast Developer permission can read state, but requires Maintainer+ permissions to modify/delete state.
* HashiCorp Terraform installed on your workstation. If you can't do that, open this repository in Gitpod.

## Setup

###

### Gitpod

[Click this link](https://gitpod.io/#https://gitlab.com/community-lores/infra/terraform-config) to open this repository in Gitpod. You maybe prompted to configure required variables if prompted.

## Required variables for setup script

Please handle these secrets with care, and in case you loose them or got leaked, please immediately email <emergency-meeting@community-lores.gq> (in the future it'll be <emergency-meeting@lores.community>)[^3]

* `GITLAB_TF_STATE_PAT` - GitLab SaaS PAT for accessing the remote Terraform state. Atleast `api` scope is required.
* `GITLAB_TOKEN` - GitLab SaaS API token of either our shared team account (@thepinsteam) or your own PAT.[^2]
* `GITHUB_TOKEN` - GitHub.com API token, same as  `GITLAB_TOKEN`. For scopes, you need `repo`, `admin:org`, and `delete_repo` scopes.
    * **WARNING**: The `admin:org` and `delete_repo` sounds an *very dangerous operation*. For most cases, only `write:org` is probably what you need.


[^1]: Please esclate permissions at your own risk. You must have access to `permisson-esclation-api-keys` project in Doppler to get them.
[^2]: If you have your own PATs + Maintainer+ (in GitHub: Maintain+ for repositories, team admin for GitHub teams and subteams, Owner for org-wide management) permissions, you should be fine.
[^3]: These email addresses will route to one of Recap Time's internal issue tracker AKA The Emergency Meeting Room For Lost Devices and Leaked Secrets as **confidential GitLab issue** without any connection to you (please tag yourself in the issue body or on the automated reply to get access).
