variable "group_id" {
  description = "GitLab SaaS group ID, because it's pain the encode group path into URL-friendly ones."
}
variable "username" {
  description = "GitLab SaaS username"
}
variable "access_level" {
  description = "Access level for an specific user in an GitLab project/group."
  default = "reporter"
}

terraform {
  required_providers {
    gitlab = {
      source = "gitlabhq/gitlab"
    }
  }
}

data "gitlab_user" "user" {
  username = var.username
}

resource "gitlab_group_membership" "group_member" {
  group_id     = var.group_id
  user_id      = data.gitlab_user.user.user_id
  access_level = var.access_level
}
